package ru.smochalkin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IRepositoryDto;
import ru.smochalkin.tm.dto.UserDto;

import java.util.List;
import java.util.Optional;

public interface IUserDtoRepository extends IRepositoryDto<UserDto> {

    int getCount();

    void clear();

    @NotNull
    List<UserDto> findAll();

    @Nullable
    UserDto findByLogin(@NotNull String login);

    @Nullable
    UserDto findById(@Nullable String id);

    void removeByLogin(@NotNull String login);

    void removeById(@Nullable String id);
    
}
