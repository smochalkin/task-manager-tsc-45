package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.endpoint.IAdminEndpoint;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public Result createUser(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) {
        serviceLocator.getSessionService().validate(sessionDto, Role.ADMIN);
        try {
            serviceLocator.getUserService().create(login, password, email);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result removeUserById(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                 @WebParam(name = "userId") @Nullable final String userId
    ) {
        serviceLocator.getSessionService().validate(sessionDto, Role.ADMIN);
        try {
            serviceLocator.getUserService().removeById(userId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result removeUserByLogin(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                    @WebParam(name = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(sessionDto, Role.ADMIN);
        try {
            serviceLocator.getUserService().removeByLogin(login);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result lockUserByLogin(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                  @WebParam(name = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(sessionDto, Role.ADMIN);
        try {
            serviceLocator.getUserService().lockUserByLogin(login);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result unlockUserByLogin(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                    @WebParam(name = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(sessionDto, Role.ADMIN);
        try {
            serviceLocator.getUserService().unlockUserByLogin(login);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public List<UserDto> findAllUsers(@WebParam(name = "session") @NotNull final SessionDto sessionDto) {
        serviceLocator.getSessionService().validate(sessionDto, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public List<SessionDto> findAllSessionsByUserId(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                                    @WebParam(name = "userId") @Nullable final String userId
    ) {
        serviceLocator.getSessionService().validate(sessionDto, Role.ADMIN);
        return serviceLocator.getSessionService().findAllByUserId(userId);
    }

    @Override
    @WebMethod
    public Result closeAllSessionsByUserId(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                           @WebParam(name = "userId") @Nullable final String userId
    ) {
        serviceLocator.getSessionService().validate(sessionDto, Role.ADMIN);
        try {
            serviceLocator.getSessionService().closeAllByUserId(userId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
