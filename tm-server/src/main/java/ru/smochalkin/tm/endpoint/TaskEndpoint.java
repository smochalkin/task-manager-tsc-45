package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.endpoint.ITaskEndpoint;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result createTask(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().create(sessionDto.getUserId(), name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeTaskStatusById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().updateStatusById(sessionDto.getUserId(), id, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeTaskStatusByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().updateStatusByIndex(sessionDto.getUserId(), index, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeTaskStatusByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().updateStatusByName(sessionDto.getUserId(), name, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDto> findTaskAllSorted(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "sort") @NotNull final String strSort
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getTaskService().findAll(sessionDto.getUserId(), strSort);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDto> findTaskAll(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getTaskService().findAll(sessionDto.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public TaskDto findTaskById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getTaskService().findById(sessionDto.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public TaskDto findTaskByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getTaskService().findByName(sessionDto.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public TaskDto findTaskByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getTaskService().findByIndex(sessionDto.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeTaskById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().removeById(sessionDto.getUserId(), id);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeTaskByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().removeByName(sessionDto.getUserId(), name);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeTaskByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().removeByIndex(sessionDto.getUserId(), index);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result clearTasks(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().clear(sessionDto.getUserId());
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateTaskById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().updateById(sessionDto.getUserId(), id, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateTaskByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getTaskService().updateByIndex(sessionDto.getUserId(), index, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDto> findTasksByProjectId(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getProjectTaskService().findTasksByProjectId(sessionDto.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result bindTaskByProjectId(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectTaskService().bindTaskByProjectId(sessionDto.getUserId(), projectId, taskId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result unbindTaskByProjectId(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectTaskService().unbindTaskByProjectId(sessionDto.getUserId(), projectId, taskId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
