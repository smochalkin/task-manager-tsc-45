package ru.smochalkin.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    static String salt(@NotNull final String value,
                       @NotNull final String secret,
                       @NotNull final Integer iteration) {
        @NotNull String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    @SneakyThrows
    static String salt(@NotNull final Object value,
                       @NotNull final String secret,
                       @NotNull final Integer iteration) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(value);
        return salt(json, secret, iteration);
    }

    @NotNull
    static String md5(String value) {
        try {
            @NotNull MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (final byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
