package ru.smochalkin.tm.api.model;

public interface IWBS extends IHasCreated, IHasStartDate, IHasEndDate, IHasName, IHasStatus {
}
