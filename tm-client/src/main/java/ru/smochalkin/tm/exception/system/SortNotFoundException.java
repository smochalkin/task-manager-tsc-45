package ru.smochalkin.tm.exception.system;

import ru.smochalkin.tm.exception.AbstractException;

public final class SortNotFoundException extends AbstractException {

    public SortNotFoundException() {
        super("Error! Sort option is incorrect.");
    }

}
